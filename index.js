let http = require("http");

http.createServer(function(request, response) {
	// GET Request
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content_Type': 'text/plain'});
		response.end('Welcome to profile');
	}
	else if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content_Type': 'text/plain'});
		response.end("Here's our courses available:");
	}
	else if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content_Type': 'text/plain'});
		response.end('Add a courses to our resources');
	}
	else if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content_Type': 'text/plain'});
		response.end('Update a course to our resources');
	}
	else if(request.url == "/archievecourses" && request.method == "DELETE"){
		response.writeHead(200, {'Content_Type': 'text/plain'});
		response.end('Archieve courses to our resources');
	}
	else{
		response.writeHead(200, {'Content_Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}
	
}).listen(4000);

console.log('Server is running at localhost:4000');	